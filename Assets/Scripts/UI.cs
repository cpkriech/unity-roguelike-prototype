﻿using UnityEngine;
using System.Collections;

public class UI : MonoBehaviour {

	public UISlider healthbar;
	public LevelGenerator levelgen;
	public UITextList textlist;
	//public UITextList abilitylist;
	public UITextList componentlist;
	public UILabel playername;
	public UILabel info;
	public GameObject AbilityMenu;
	public GameObject popup;
	public UILabel popupText;

	private CharacterStats playerstats;

	void Start () {
		textlist.Add ("One dark morning, in a dark ruin in the heart of the Forgotten Mountians, Helen Strong faces her most hated enemies:" +
		              " the Unified Forces of Beastfolk...");
		componentlist.Add ("Loading...");
	}
	
		// Update is called once per frame
	void Update () {
		if (playerstats == null) {
			playerstats = levelgen.playermgr.player.GetComponent<CharacterStats> ();
			componentlist.Clear ();
			for(int i = 0; i < playerstats.components.Count; i++){
				componentlist.Add(playerstats.components[i].name);	
			}
		}
		healthbar.value = ((float)playerstats.currenthealth / (float)playerstats.maxhealth); 
		if (Input.GetMouseButtonDown (0)) {
			RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint  (Input.mousePosition), Vector2.zero);	
			if (hit.collider != null) {
				//popup.transform.position = Camera.allCameras[1].ScreenToWorldPoint(hit.transform.position);
				popup.SetActive (true);
				CharacterStats npcstats = hit.collider.gameObject.GetComponent<CharacterStats>();
				string npcName = "";
				for(int k = 0; k<npcstats.components.Count;k++){
					npcName = npcName + " " + npcstats.components[k].name;
				}
				popupText.text = npcName + 
					"\n \n ATK: " + npcstats.attack + 
					"\n MAG: " + npcstats.magic +
					"\n SPD: " + npcstats.speed +
					"\n Health: " + npcstats.currenthealth + "/" + npcstats.maxhealth;
			} 
			else {
				popup.SetActive(false);
			};

		}
		playername.text = playerstats.gameObject.name;
		info.text = "ATK: " + playerstats.attack + "\nMAG: " + playerstats.magic + "\nSPD: " + playerstats.speed;


	}
}

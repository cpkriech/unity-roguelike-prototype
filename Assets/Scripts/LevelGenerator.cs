﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using SpriteTile;

public class LevelGenerator : MonoBehaviour {

	public int size_x;
	public int size_y;
	public int room_length;
	public int room_width;
	public int fov_range;
	public DTileMap dtilemap;
	public NPCManager npcgen;
	public ComponentManager commgr;
	public int numNPCs;
	public GameObject playerObject;
	public UI ui;

	public PlayerManager playermgr;

	public SaveData savedata;
	public bool cameraAttached = false;

	// Use this for initialization
	void Awake() {

	}

	void Start () {
		GameObject globals = GameObject.FindGameObjectWithTag ("Globals");
		if (!globals.GetComponent<Globals> ().cameraAttached) {
			Tile.SetCamera ();
			globals.GetComponent<Globals> ().cameraAttached = true;
		}
		NewPlayer();
		Tile.NewLevel (new Int2 (size_x, size_y), 0,1f, 0.0f, LayerLock.None);
		savedata = new SaveData ();
		dtilemap = new DTileMap (size_x, size_y, room_length, room_width, fov_range);
		if (PlayerPrefs.GetInt ("New Game") == 1) {
			NewGame();		
		}
		else {
			LoadGame ();
		}
	}

	protected void NewGame(){
		dtilemap.GenerateMap ();
		PlacePlayer ();
		SpawnNPCs ();
		MapUpdate ();
	}

	protected void LoadGame(){
		savedata.LoadLevel ();
		dtilemap.LoadMap (savedata.map, savedata.tiledata);
		playermgr.LoadPlayer (savedata.player);
		npcgen.LoadAllNPCs (savedata.npcs);
		MapUpdate ();
	}

	public void Save() {
		savedata.SaveLevel (dtilemap.map_data, dtilemap.map, npcgen.GetAllCharacterData(), playermgr.GetCharacterData());
	}
	public void DrawLevel() {

		for (int y = 0; y < size_y; y++) {
			for (int x = 0; x < size_x; x++) {
				//Tile.DeleteTile(new Int2(x,y), 0);
				int set = 0;
				int id = 0;
				set = dtilemap.map[x,y].tileGraphicSet;
				if (dtilemap.map[x,y].isVisible) {
					id = dtilemap.map[x,y].tileGraphicId; }
				else if (!dtilemap.map[x,y].isVisible && dtilemap.map[x,y].HasBeenVisible){
					id = dtilemap.map[x,y].tileGraphicIdDark;}
				else if (!dtilemap.map[x,y].isVisible && !dtilemap.map[x,y].HasBeenVisible) {
					id = 8; }
				Tile.SetTile (new Int2 (x, y),0, set, id);

				//f (!dtilemap.map[x,y].isVisible && dtilemap.map[x,y].HasBeenVisible) 
					//id  = dtilemap.map[x,y].tileGraphicIdDark;

				//id = 5;
			}
		}
		npcgen.UpdateAllNPCVisibility ();
	}
	public bool isWalkable(int x, int y){
		if (npcgen.npcCount > 0) {
			for (int i = 0; i < npcgen.npcCount; i++) {
				if (npcgen.npcs [i].transform.position == new Vector3 (x, y) && npcgen.npcs [i].GetComponent<NpcController> ().isAlive)
					return false;
			}
		}
		//if (playercon.transform.position == new Vector3 (x, y))
			//return false;
		if (x >= size_x || y >= size_y)
			return false;
		return dtilemap.map[x,y].isWalkable;
	}
	public bool isBlocking(int x, int y){
		return dtilemap.isBlocking(x,y);
	}
	public bool isInPlayerFOV(int x, int y){
	//returns if target tile is in player fov. Replacement until monster fov becomes a thing.
		return dtilemap.map [x, y].isVisible;
	}
	public Vector2 RandomWalkableTile() {
		int ranx = Random.Range (0, size_x);
		int rany = Random.Range (0, size_y);
		while (!isWalkable(ranx, rany)){
			ranx = Random.Range (0, size_x);
			rany = Random.Range (0, size_y);
		}
		return new Vector2 (ranx, rany);
	}
	void NewPlayer(){
		playermgr.CreateNewPlayer ();
	}
	void PlacePlayer(){
		Vector2 randomTile = RandomWalkableTile ();
		playermgr.player.GetComponent<PlayerController>().MovePlayerTo ((int)randomTile.x, (int)randomTile.y);
		MapUpdate ();
	}

	void SpawnNPCs(){
		for (int x = 0; x < numNPCs; x++) {
			int index = npcgen.CreateNewRandomNPC ();
			Vector2 randomTile = RandomWalkableTile ();
			npcgen.npcs [index].GetComponent<NpcController>().MoveNPCTo ((int)randomTile.x, (int)randomTile.y);
		}
	}
	public void MapUpdate(){
		dtilemap.UpdateFov ((int)playermgr.player.transform.position.x, (int)playermgr.player.transform.position.y);
		DrawLevel ();
	}
}

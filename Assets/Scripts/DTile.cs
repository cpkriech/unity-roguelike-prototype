﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class DTile {

	public string name;
	public bool isWalkable;
	public int tileGraphicSet;	
	public int tileGraphicId;	
	public int tileGraphicIdDark;
	public bool isVisible  = false;
	public bool HasBeenVisible = false;
	
	public DTile(string name, bool isWalkable, int tileGraphicSet, int tileGraphicId, int tileGraphicIdDark){
		this.name = name;
		this.isWalkable = isWalkable;
		this.tileGraphicSet = tileGraphicSet;
		this.tileGraphicId = tileGraphicId;
		this.tileGraphicIdDark = tileGraphicIdDark;
	}
}
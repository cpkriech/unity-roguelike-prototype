﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using System.Collections.Generic;

[System.Serializable]
public class SaveData : MonoBehaviour {
	public int[,] map;
	public DTile[,] tiledata;
	public List<CharacterData> npcs;
	public CharacterData player;
	
	public void SaveLevel (int [,] smap, DTile[,] stiledata, List<CharacterData> snpcs, CharacterData splayer) {
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath +"/" + "GameInfo.dat");
		LevelData data = new LevelData (smap, stiledata, snpcs, splayer);
		bf.Serialize(file, data);
		file.Close ();
	}
	
	public void LoadLevel () {
		
		if (File.Exists (Application.persistentDataPath +"/" + "GameInfo.dat")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath +"/" + "GameInfo.dat", FileMode.Open);
			LevelData data = (LevelData)bf.Deserialize(file);
			file.Close ();

			map = data.map;
			tiledata = data.tiledata;
			npcs = data.npcs;
			player = data.player;
			}
	}

	[System.Serializable]
	class LevelData {
		//Data for a specific level. Currently holds player data as well, will change when required
		public int[,] map;
		public DTile[,] tiledata;
		public List<CharacterData> npcs;
		public CharacterData player;
		
		public LevelData( int [,] map, DTile[,] tiledata, List<CharacterData> npcs, CharacterData player) {
			this.map = map;
			this.tiledata = tiledata;
			this.npcs = npcs;
			this.player = player;
		}
	}
}



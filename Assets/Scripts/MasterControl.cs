﻿using UnityEngine;
using System.Collections;

public class MasterControl : MonoBehaviour {
	public LevelGenerator levelgen;
	public PlayerController playercon = null;
	protected Queue timeQueue;
	public bool playerTookTurn = true;

	void Awake() {
		InitializeTimeQueue ();
	}

	void Update () {
		if (playercon == null)
			playercon = levelgen.playermgr.player.GetComponent<PlayerController> ();
		if (!playerTookTurn) {
			playerTookTurn = playercon.PlayerInput();
		} 
		else if (playerTookTurn){
			while (playerTookTurn) Tick ();
		}
	}

	protected void Tick(){
		if (timeQueue.Count > 0) {
			TimeNode curr = (TimeNode)timeQueue.Peek ();
			//removes anything dead from queue, stops them from acting
			curr.charstats.energy = curr.charstats.energy + curr.charstats.speed;
			timeQueue.Enqueue(timeQueue.Dequeue());
			TimeNode next = (TimeNode)timeQueue.Peek ();
			if(next.isPlayer && next.charstats.energy >= 1000) playerTookTurn = false;
			if (curr.charstats.energy >= 1000){
				if (!curr.isPlayer) {
					int energyCost = levelgen.npcgen.TakeTurn(curr.index);
					curr.charstats.energy = curr.charstats.energy - energyCost;
					}
				else if (curr.isPlayer && playerTookTurn){
				//player action
					int energyCost = playercon.PlayerAction();
					curr.charstats.energy = curr.charstats.energy - energyCost;
				}
				else {
				}
			}			
		}
	}
	protected void InitializeTimeQueue(){
		timeQueue = new Queue ();
	}
	public void RegisterEntityPlayer(){
		//energy should be zero
		timeQueue.Enqueue (new TimeNode (true, 0, levelgen.playermgr.player.GetComponent<CharacterStats> ()));
	}
	public void RegisterEntityNPC(int index){
		//energy should be zero
		timeQueue.Enqueue (new TimeNode (false, index, levelgen.npcgen.npcs[index].GetComponent<CharacterStats> ()));
	}
	public class TimeNode{
		public bool isPlayer;
		public int index;
		public CharacterStats charstats;

		public TimeNode(bool isPlayer, int index, CharacterStats charstats){
			this.isPlayer = isPlayer;
			this.index = index;
			this.charstats = charstats;
		}
	}
}
//
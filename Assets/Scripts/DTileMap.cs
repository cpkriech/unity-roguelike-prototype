﻿using UnityEngine;
using System.Collections.Generic;
using SilverlightShadowCasting;

public class DTileMap {

	
	public List<DTile> tileTypes;
	
	/*OLD
	 * 0 = unknown
	 * 1 = floor
	 * 2 = wall
	 * 3 = stone
	 */
	
	/*
	 * 0 = Ball (Player temp Sprite)
	 * 1 = Floor
	 * 2 = unknown
	 * 3 = Wall
	 * 4 = Stone
	 * 
	 */
	
	public enum Tiles {Floor, Unknown, Wall, Stone};

	public void InitTiles() {
		tileTypes = new List<DTile>();

		/*DTile tile = new DTile();
		tile.name = "Floor";
		tile.isWalkable = true;
		tile.tileGraphicSet = 1;
		tile.tileGraphicId = 13;
		tile.tileGraphicIdDark = 14;
		tile.isVisible = false;
		tile.HasBeenVisible = false;
		tileTypes.Add (tile);

		tile = new DTile ();
		tile.name = "Unknown";
		tile.isWalkable = false;
		tile.tileGraphicSet = 1;
		tile.tileGraphicId = 5;
		tile.tileGraphicIdDark = 5;
		tile.isVisible = false;
		tile.HasBeenVisible = false;
		tileTypes.Add (tile);
		
		tile = new DTile();
		tile.name = "Wall";
		tile.isWalkable = false;
		tile.tileGraphicSet = 1;
		tile.tileGraphicId = 11;
		tile.tileGraphicIdDark = 12;
		tile.isVisible = false;
		tile.HasBeenVisible = false;
		tileTypes.Add (tile);
		
		tile = new DTile();
		tile.name = "Stone";
		tile.isWalkable = false;
		tile.tileGraphicSet = 1;
		tile.tileGraphicId = 11;
		tile.tileGraphicIdDark = 12;
		tile.isVisible = false;
		tile.HasBeenVisible = false;
		tileTypes.Add (tile);*/
	}

	protected class DRoom {
		public int left;
		public int top;
		public int width;
		public int height;
		
		public bool isConnected=false;
		
		public int right {
			get {return left + width - 1;}
		}
		
		public int bottom {
			get { return top + height - 1; }
		}
		
		public int center_x {
			get { return left + width/2; }
		}
		
		public int center_y {
			get { return top + height/2; }
		}
		
		public bool CollidesWith(DRoom other) {
			if( left > other.right-1 )
				return false;
			
			if( top > other.bottom-1 )
				return false;
			
			if( right < other.left+1 )
				return false;
			
			if( bottom < other.top+1 )
				return false;
			
			return true;
		}		
	}


	
/////////////////////////////////////////////////////////


	int size_x;
	int size_y;
	int room_length;
	int room_width;
	int fov_radius;


	int[,] blocking_map;
		
		//raw map data
	public int[,] map_data;

	public DTile[,] map;
	
	List<DRoom> rooms;

	public DTileMap(int size_x, int size_y, int room_length, int room_width, int fov_radius) {

		this.size_x = size_x;
		this.size_y = size_y;
		this.room_length = room_length;
		this.room_width = room_width;
		this.fov_radius = fov_radius;
	}
	public void GenerateMap () {
		GenerateMapData ();
		InitTileMap ();
		GenerateFov ();

	}
	public void LoadMap(int[,] map_data, DTile[,] map){
		this.map_data = map_data;
		this.map = map;
		GenerateFov ();
	}
	protected void GenerateMapData() {
		DRoom r;
		map_data = new int[size_x,size_y];

		
		for(int x=0;x<size_x;x++) {
			for(int y=0;y<size_y;y++) {
				map_data[x,y] = (int)Tiles.Stone;
			}
		}
		
		rooms = new List<DRoom>();
		
		int maxFails = 10;
		
		while(rooms.Count < 50) {
			int rsx = Random.Range(4,room_length);
			int rsy = Random.Range(4,room_width);
			
			r = new DRoom();
			r.left = Random.Range(0, size_x - rsx);
			r.top = Random.Range(0, size_y - rsy);
			r.width = rsx;
			r.height = rsy;
			
			if(!RoomCollides(r)) {			
				rooms.Add (r);
			}
			else {
				maxFails--;
				if(maxFails <=0)
					break;
			}
		}
		
		foreach(DRoom r2 in rooms) {
			MakeRoom(r2);
		}
		

		for(int i=0; i < rooms.Count; i++) {
			if(!rooms[i].isConnected) {
				int j = Random.Range(1, rooms.Count);
				MakeCorridor(rooms[i], rooms[(i + j) % rooms.Count ]);
			}
		}	
		MakeWalls();	

	}

	void InitTileMap(){
		InitTiles ();
		map = new DTile[size_x,size_y];
		for(int x=0;x<size_x;x++) {
			for(int y=0;y<size_y;y++) {
				if (map_data[x,y] == (int)Tiles.Floor)
					map[x,y] = new DTile("Floor", true, 0, 6, 7);
				if (map_data[x,y] == (int)Tiles.Stone)
					map[x,y] = new DTile("Floor", false, 0, 1, 1);
				if (map_data[x,y] == (int)Tiles.Unknown)
					map[x,y] = new DTile("Floor", false, 0, 5, 5);
				if (map_data[x,y] == (int)Tiles.Wall)
					map[x,y] = new DTile("Floor", false, 0, 0, 1);
			}
		}
	}

	void GenerateFov(){
		//initialze fov and create first map
		blocking_map = new int[size_x, size_y];
		//genrate int map of blocking and not blocking (1 = blocking, 0 = not blocking)
		for(int x=0;x<size_x;x++) {
			for(int y=0;y<size_y;y++) {
				if(isBlocking (x,y))
					blocking_map[x,y] = 1;
				else 
					blocking_map[x,y] = 0;
			}
		}
	}

	public void UpdateFov (int playerx, int playery){
		bool[,] lit = new bool[size_x, size_y];
		ShadowCaster.ComputeFieldOfViewWithShadowCasting(
			playerx, playery, fov_radius,
			(x1, y1) => blocking_map[x1, y1] == 1,
			(x2, y2) => { lit[x2, y2] = true; });

		for (int x=0; x<size_x; x++) {
			for (int y=0; y<size_y; y++) {
				map[x,y].isVisible = false;
				if (lit[x,y]) {
					map[x,y].isVisible = true;
					map[x,y].HasBeenVisible = true;
				}
			}
		}
	}
	
	public bool isBlocking(int x, int y){
		//will be changed when tile formats change 
		if (x >= size_x || y >= size_y)
				return false;
		return (!map[x,y].isWalkable);
	}




	bool RoomCollides(DRoom r) {
		foreach(DRoom r2 in rooms) {
			if(r.CollidesWith(r2)) {
				return true;
			}
		}	
		return false;
	}
	
	public DTile GetTileAt(int x, int y) {
		return map[x,y];
	}

	void MakeRoom(DRoom r) {
		
		for(int x=0; x < r.width; x++) {
			for(int y=0; y < r.height; y++){
				if(x==0 || x == r.width-1 || y==0 || y == r.height-1) {
					map_data[r.left+x,r.top+y] = (int)Tiles.Wall;
				}
				else {
					map_data[r.left+x,r.top+y] = (int)Tiles.Floor;
				}
			}
		}
		
	}



	void MakeCorridor(DRoom r1, DRoom r2) {
		int x = r1.center_x;
		int y = r1.center_y;
		
		while( x != r2.center_x) {
			map_data[x,y] = (int)Tiles.Floor;
			
			x += x < r2.center_x ? 1 : -1;
		}
		
		while( y != r2.center_y ) {
			map_data[x,y] = (int)Tiles.Floor;
			
			y += y < r2.center_y ? 1 : -1;
		}
		
		r1.isConnected = true;
		r2.isConnected = true;
		
	}


	
	void MakeWalls() {
		for(int x=0; x< size_x;x++) {
			for(int y=0; y< size_y;y++) {
				if(map_data[x,y]==(int)Tiles.Stone && HasAdjacentFloor(x,y)) {
					map_data[x,y]=(int)Tiles.Wall;
				}
			}
		}
	}



	bool HasAdjacentFloor(int x, int y) {
		if( x > 0 && map_data[x-1,y] == (int)Tiles.Floor )
			return true;
		if( x < size_x-1 && map_data[x+1,y] == (int)Tiles.Floor )
			return true;
		if( y > 0 && map_data[x,y-1] == (int)Tiles.Floor  )
			return true;
		if( y < size_y-1 && map_data[x,y+1] == (int)Tiles.Floor  )
			return true;

		if( x > 0 && y > 0 && map_data[x-1,y-1] == (int)Tiles.Floor  )
			return true;
		if( x < size_x-1 && y > 0 && map_data[x+1,y-1] == (int)Tiles.Floor  )
			return true;
		
		if( x > 0 && y < size_y-1 && map_data[x-1,y+1] == (int)Tiles.Floor  )
			return true;
		if( x < size_x-1 && y < size_y-1 && map_data[x+1,y+1] == (int)Tiles.Floor  )
			return true;
		return false;
	}
}



﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	public GameObject globals;

	void Start () {
		if (GameObject.FindGameObjectWithTag("Globals") == null)
			globals = (GameObject)Instantiate(Resources.Load ("Globals"));
	}

	void Update () {
	
	}

	public void NewGameButtonClick()
	{
		PlayerPrefs.SetInt ("New Game", 1);
		Application.LoadLevel (1);
	}

	public void LoadGameButtonClick(){
		PlayerPrefs.SetInt ("New Game", 0);
		Application.LoadLevel (1);
	}

}

﻿using UnityEngine;
using System.Collections.Generic;

public class ComponentManager : MonoBehaviour {
	public CharComponent[] speciesList;
	public CharComponent[] templateList;
	public CharComponent[] modifierList;
	public CharComponent[] classList;
	//public list<Component> AbilityList;
	//public list<Component> ModifierList;
	
	//consts	
	public readonly int SPECIES_DWARF  = 0;
	public readonly int SPECIES_ELF    = 1;
	public readonly int SPECIES_ORC    = 2;
	public readonly int SPECIES_KOBOLD = 3;
	public readonly int SPECIES_RAT    = 4;
	public readonly int SPECIES_SKULL  = 5;
	public readonly int SPECIES_BALL   = 6;


	//Modifies many stats, adds abilities, major.
	public readonly int TEMPLATE_VAMPIRIC   = 0;
	public readonly int TEMPLATE_YOUNG      = 1;
	public readonly int TEMPLATE_ANCIENT    = 2;
	public readonly int TEMPLATE_LARGE      = 3;
	public readonly int TEMPLATE_ENORMOUS   = 4;
	public readonly int TEMPLATE_TINY       = 5;


	//Modifies one or two stats, minor
	public readonly int MODIFIER_STRONG = 0;
	public readonly int MODIFIER_QUICK  = 1;
	public readonly int MODIFIER_SLOW   = 2;
	public readonly int MODIFIER_CLAWED = 3;

	//Classes, big modifiers. In full game, would be very powerful, and rare to find on an npc. Would add abilities and cool stuff.
	public readonly int CLASS_BRAWLER   = 0;
	public readonly int CLASS_SOLDIER   = 1;
	public readonly int CLASS_WIZARD    = 2;
	public readonly int CLASS_AGENT     = 3;
	public readonly int CLASS_CULTIST   = 4;
	public void Awake(){
		PopulateLists ();
	}
	//Orc, Elf, etc. 
	//public GetSpeciesComponent(){
	//}
	//Vampiric, Berserking, etc. Changes types. Big changes.
	//public GetTemplateComponent(){
	//}

	//Class-types. Add abilities.
	//Modifiers, little changes. Mostly used by player. Can be used by any.
	private void PopulateLists(){
		PopulateSpecies ();
		PopulateTemplates ();
		PopulateModifers();
		PopulateClasses ();
	}
	private void PopulateSpecies(){
		speciesList = new CharComponent[100];
		speciesList[SPECIES_ELF]    = new CharComponent("Elf",    
		                                                BaseAttack: 10, BaseMagic: 15, BaseHealth: 8, BaseSpeed: 105);
		speciesList[SPECIES_DWARF]  = new CharComponent("Dwarf",
		                                                BaseAttack: 10, BaseMagic: 11, BaseHealth: 12, BaseSpeed: 100);
		speciesList[SPECIES_ORC]    = new CharComponent("Orc",
		                                                BaseAttack: 12, BaseMagic: 10, BaseHealth: 12, BaseSpeed: 100);
		speciesList[SPECIES_KOBOLD] = new CharComponent ("Kobold",
		                                                BaseAttack: 8, BaseMagic: 6, BaseHealth: 8, BaseSpeed: 110);
		speciesList[SPECIES_RAT]    = new CharComponent("Rat",
		                                                BaseAttack: 4, BaseMagic: 1, BaseHealth: 5, BaseSpeed: 125);
		speciesList[SPECIES_SKULL]    = new CharComponent("Flame Skull",
		                                                BaseAttack: 5, BaseMagic: 10, BaseHealth: 7, BaseSpeed: 100, MagicAttack:true);
		speciesList[SPECIES_BALL]    = new CharComponent("Living Spell",
		                                                 BaseAttack: 5, BaseMagic: 20, BaseHealth: 4, BaseSpeed: 90, AttackRange: 3, MagicAttack:true);
	}
	private void PopulateTemplates(){
		templateList = new CharComponent[100];
		//templateList[TEMPLATE_VAMPIRIC] = new CharComponent
		templateList [TEMPLATE_YOUNG] = new CharComponent ("Young", ModAttack: -3, ModHealth: -30);
		templateList[TEMPLATE_ANCIENT] = new CharComponent ("Young", ModAttack: -5, ModHealth: -10);
		templateList[TEMPLATE_LARGE] = new CharComponent ("Large", ModSpeed: -5, ModAttack: 3, ModHealth: 10);
		templateList [TEMPLATE_ENORMOUS] = new CharComponent ("Enormous", ModSpeed: -10, ModAttack: 5, ModHealth: 30);
		//templateList[TEMPLATE_TINY] = new CharComponent

	}
	private void PopulateModifers(){
		modifierList = new CharComponent[100];
		modifierList[MODIFIER_STRONG] = new CharComponent ("Mighty", ModAttack: 5);
		modifierList[MODIFIER_QUICK]  = new CharComponent ("Quick",ModSpeed:  5);
		modifierList[MODIFIER_SLOW]   = new CharComponent ("Slow", ModSpeed: -50);
		//modifierList[MODIFIER_CLAWED]  = new CharComponent (ModAttack: 5);
	}

	private void PopulateClasses(){
		classList = new CharComponent[100];
		classList [CLASS_BRAWLER] = new CharComponent ("Brawler", ModAttack: 10, ModMagic: -3, ModHealth: 100);
		classList [CLASS_SOLDIER] = new CharComponent ("Soldier", ModAttack: 5, ModSpeed: 20);
		classList [CLASS_WIZARD]  = new CharComponent ("Wizard", ModMagic: 20, ModHealth: -30, MagicAttack:true);
		classList [CLASS_AGENT]   = new CharComponent ("Agent", ModAttack: 5, ModSpeed: 50);
		classList [CLASS_CULTIST] = new CharComponent ("Cultist", ModAttack: -5, ModMagic: 5);
	}

	public CharComponent GetRandomComp(){
		int ranType = Random.Range (0,10);
		if (ranType <= 4) {
			return GetRandomTemplate ();
		}
		else if (ranType <= 8 && ranType > 4) {
			return GetRandomModifier ();
		}
		else if (ranType == 9) {
			return GetRandomClass();
		}
		else{
			//returns if bad number generated.
			return new CharComponent("null");
		}
	}

	public CharComponent GetRandomTemplate(){
		//this is bad code, for prototype.
		int ranIndex = Random.Range (0,4);
		switch(ranIndex){
		case 0:
			return templateList [TEMPLATE_YOUNG];
			break;
		case 1:
			return templateList [TEMPLATE_ANCIENT];
			break;
		case 2:
			return templateList [TEMPLATE_LARGE];
			break;
		case 3:
			return templateList [TEMPLATE_ENORMOUS];
			break;
		default:
			//returns if bad number generated.
			return new CharComponent("null template");
		}
		return new CharComponent("null template");
	}
	public CharComponent GetRandomModifier(){
		int ranIndex = Random.Range (0,3);
		switch(ranIndex){
		case 0:
			return modifierList [MODIFIER_STRONG];
			break;
		case 1:
			return modifierList [MODIFIER_QUICK];
			break;
		case 2:
			return modifierList [MODIFIER_SLOW];
			break;
		default:
			//returns if bad number generated.
			return new CharComponent("null mod");
		}
		return new CharComponent("null mod");
	}
	public CharComponent GetRandomClass(){
		int ranIndex = Random.Range (0,5);
		switch(ranIndex){
		case 0:
			return classList [CLASS_BRAWLER];
			break;
		case 1:
			return classList [CLASS_SOLDIER];
			break;
		case 2:
			return classList [CLASS_WIZARD];
			break;
		case 3:
			return classList [CLASS_AGENT];
			break;
		case 4:
			return classList [CLASS_CULTIST];
			break;
		default:
			//returns if bad number generated.
			return new CharComponent("null class");
		}
		return new CharComponent("null class");
	}

	public CharComponent GetSpecies(int index){
		return speciesList [index];
	}
	public CharComponent GetTemplate(int index){
		return templateList [index];
	}
	public CharComponent GetModifier(int index){
		return modifierList [index];
	}
	public CharComponent GetClass(int index){
		return classList [index];
	}
}
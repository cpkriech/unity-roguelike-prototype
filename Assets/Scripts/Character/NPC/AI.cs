﻿using UnityEngine;
using System.Collections;

public class AI{

	NpcController npccon;

	protected int damage;
	protected int moraleBreak;
	//0 - 49 npc will charge, 50+ they will stand and shoot
	protected int chargeProbability = 50;
	protected int retreatProbability = 50;
	protected int weaponRange;

	public AI (NpcController npccon){
		this.npccon = npccon;
	}
	
	public string AiAction(int damage = 0, int moraleBreak = 10000, int weaponRange = 1) {
		this.damage = damage;
		this.moraleBreak = moraleBreak;
		this.weaponRange = weaponRange;
		return AiLogic ();
	}

	protected string AiLogic() {
		if (!IsVisibleByPlayer ()) {
			int ran = Random.Range (0, 10);
			if(ran <= 74)
				return MoveRandom();
			else{
				return StandStill();
			}
		}
		else {
			//morale not implimented
			if (damage > moraleBreak) {
				if (CanMoveAwayFromTarget ())
					return MoveAwayFromTarget ();
				else if (CanAttackTarget ())
					return AttackTarget ();
			}
			else if (TooFarFromTarget() && CanAttackTarget() && CanMoveTowardsTarget()) {
				int ran = Random.Range(0,100);
				if (ran < chargeProbability) 
					return MoveTowardsTarget();
				else 
					return AttackTarget();
			}
			else if (TooCloseToTarget() && CanAttackTarget() && CanMoveAwayFromTarget()) {
				int ran = Random.Range(0,100);
				if (ran < retreatProbability)
					return MoveAwayFromTarget();
				else 
					return AttackTarget();
			}
			else if (CanAttackTarget()) 
				return AttackTarget();
			else if (TooFarFromTarget() && CanMoveTowardsTarget()) 
				return MoveTowardsTarget();
			else if (TooCloseToTarget() && CanMoveAwayFromTarget()) 
				return MoveAwayFromTarget();
			else 
				return StandStill ();
		}
		return StandStill ();
	}

	protected string AttackTarget() {
		return "AttackTarget";
	}
	protected string MoveAwayFromTarget() {
		return "MoveAwayFromTarget";
	}
	protected string MoveTowardsTarget() {
		return "MoveTowardsTarget";
	}
	protected string StandStill(){
		return "StandStill";
	}
	protected string MoveRandom() {
		return "MoveRandom";
	}
	protected bool CanMoveAwayFromTarget(){
		return npccon.CanMoveAwayFrom (Vector2.zero, choice: "currentTarget");
	}
	protected bool CanMoveTowardsTarget(){
		return npccon.CanMoveTowards(Vector2.zero, choice:"currentTarget");
	}
	protected bool CanAttackTarget(){
		if (npccon.Distanceto (Vector2.zero, choice: "currentTarget") <= weaponRange)
			return true;
		else return false;
	}
	protected bool TooCloseToTarget(){
		if (npccon.Distanceto (Vector2.zero, "currentTarget") < weaponRange)
			return true;
		else
			return false;
	}
	protected bool TooFarFromTarget(){
		if (npccon.Distanceto (Vector2.zero, "currentTarget") > weaponRange)
						return true;
		else
			return false;
	}
	protected bool IsVisibleByPlayer() {
		return npccon.IsVisibleByPlayer ();
	}

}

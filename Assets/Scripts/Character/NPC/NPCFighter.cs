﻿using UnityEngine;
using System.Collections;

public class NPCFighter : CharacterFighter {
	
	public override void Death(){
		SpriteRenderer spriteren = gameObject.GetComponent<SpriteRenderer> ();
		spriteren.material.SetColor ("_Color", Color.red);
		gameObject.GetComponent<NpcController>().isAlive = false;
		gameObject.name = "Corpse of " + gameObject.name;
		//GameObject npcmObject = GameObject.Find ("NPC Manager");
		//NPCManager npcmgr = npcmObject.GetComponent<NPCManager> ();
	}
}

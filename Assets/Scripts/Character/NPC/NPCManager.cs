﻿using UnityEngine;
using System.Collections.Generic;
//NPC Manager
public class NPCManager : MonoBehaviour {
	public GameObject[] npcs;
	public PlayerController playercon;
	public LevelGenerator levelgen;
	public ComponentManager commgr;
	public MasterControl mc;
	Sprite[] sprites;
	public int npcCount = 0;

	public void Awake(){
		npcs = new GameObject[100];
	}
	
	/*public void NPCsTakeTurns () {
		foreach (GameObject npc in npcs) {
			npc.GetComponent<NpcController>().TakeTurn ();
		}
	}*/

	public int TakeTurn(int index) {
		return npcs[index].GetComponent<NpcController>().TakeTurn ();
	}
	
	public int CreateNewRandomNPC (){
		CharacterData npcdata = new CharacterData ();
		int ran = Random.Range (0, 6);
		if (ran == 0) {
			npcdata = CreateNewCharacterData("Orc");
		}
		if (ran == 1) {
			npcdata = CreateNewCharacterData("Rat");
		}
		if (ran == 2) {
			npcdata = CreateNewCharacterData("Kobold");
		}
		if (ran == 3) {
			npcdata = CreateNewCharacterData("Knight");
		}
		if (ran == 4) {
			npcdata = CreateNewCharacterData ("Skull");
		}
		if (ran == 5) {
			npcdata = CreateNewCharacterData ("Ball");
		}
		//else if (ran == 1) {
		//}
		//else if (ran == 2) {
		//}
		//returns the index of the last created npc.
		npcs[npcCount] = CreateNewNPCObjectFromCharData (npcdata);
		CharacterStats npcstats = npcs[npcCount].GetComponent<CharacterStats> ();
		npcstats.index = npcCount;
		mc.RegisterEntityNPC (npcCount);
		npcCount++;
		return (npcstats.index);
	}
	
	public int LoadNPC(CharacterData npcdata) {
		npcs[npcdata.index] = CreateNewNPCObjectFromCharData (npcdata);
		CharacterData.SerVector3 sv = new CharacterData.SerVector3 (0, 0, 0);
		npcs[npcdata.index].transform.position = sv.SerVec3toVec3(npcdata.location);
		mc.RegisterEntityNPC (npcdata.index);
		npcCount++;
		return (npcdata.index);
	}

	public void LoadAllNPCs(List<CharacterData> npcdata) {
		foreach (CharacterData npc in npcdata) {
			LoadNPC (npc);		
		}
	}
	
	public CharacterData GetCharacterData(GameObject npc){
		CharacterStats charstats = npc.GetComponent<CharacterStats> ();
		return new CharacterData (npc.name, npc.transform.position, charstats.spriteIDnum, charstats.index,
		                          charstats.currenthealth, charstats.energy, charstats.components);
	}

	public List<CharacterData> GetAllCharacterData() {
		List<CharacterData> list = new List<CharacterData> ();
		for(int i = 0; i<npcCount; i++){
			list.Add (GetCharacterData(npcs[i]));
		}
		return list;
	}

	private CharacterData CreateNewCharacterData(string type) {
		List<CharComponent> components = new List<CharComponent>();
		CharacterData npcdata = new CharacterData();
		int ran = Random.Range (0, 5);
		Sprite sprite;
		switch (type) {
		case "Orc":
			components.Add (commgr.GetRandomComp());
			components.Add (commgr.GetSpecies(commgr.SPECIES_ORC));
			npcdata = new CharacterData("Orc", Vector3.zero, 2, -1, -1, 0, components);
			break;
		case "Rat":
			components.Add (commgr.GetRandomComp());
			components.Add (commgr.GetSpecies(commgr.SPECIES_RAT));
			npcdata = new CharacterData("Rat", Vector3.zero, 3, -1, -1, 0, components);
			break;
		case "Kobold":
			components.Add (commgr.GetRandomComp());
			components.Add (commgr.GetSpecies(commgr.SPECIES_KOBOLD));
			npcdata = new CharacterData("Kobold", Vector3.zero, 1, -1, -1, 0, components);
			break;
		case "Knight":
			components.Add (commgr.GetRandomComp());
			components.Add (commgr.GetSpecies(commgr.SPECIES_ELF));
			npcdata = new CharacterData("Knight", Vector3.zero, 4, -1, -1, 0, components);
			break;
		case "Skull":
			components.Add (commgr.GetRandomComp());
			components.Add (commgr.GetSpecies(commgr.SPECIES_SKULL));
			npcdata = new CharacterData("Skull", Vector3.zero, 7, -1, -1, 0, components);
			break;
		case "Ball":
			components.Add (commgr.GetRandomComp());
			components.Add (commgr.GetSpecies(commgr.SPECIES_BALL));
			npcdata = new CharacterData("Ball", Vector3.zero, 6, -1, -1, 0, components);
			break;
		}
		return npcdata;
	}

	private GameObject CreateNewNPCObjectFromCharData(CharacterData npcdata){
		GameObject npc = (GameObject)Instantiate(Resources.Load ("Character"));
		npc.name = npcdata.name;
		SpriteRenderer spriteren = npc.GetComponent<SpriteRenderer> ();
		spriteren.sortingLayerName = "Characters";
		spriteren.sprite = AssignSprites (npcdata.spriteIDnum);
		NpcController npccon = npc.AddComponent<NpcController> ();
		NPCFighter npcfighter = npc.AddComponent<NPCFighter> ();
		CharacterStats charstats = npc.AddComponent<CharacterStats> ();
		charstats.spriteIDnum = npcdata.spriteIDnum;
		//if new character, will be changed later, if loaded, should be correct.
		charstats.index = npcdata.index;
		npccon.AssignScripts (levelgen, playercon);
		npcfighter.AssignScripts ();
		charstats.AssignComponents (npcdata.components);
		if (npcdata.currentHealth > 0)
			charstats.currenthealth = npcdata.currentHealth;
		charstats.energy = npcdata.currentHealth;
		return npc;
	}

	public Sprite AssignSprites(int x){
		/*FileReader.SpriteID sID = fr.getSpriteID(x);
		if (sID.index != -1) {
			Sprite[] s = Resources.LoadAll<Sprite>(sID.path);
			return s[sID.index];
		}
		else {
			return Resources.Load<Sprite>(sID.path);
		}


		1Kobold:kobold)
		2Orc:npc(2)
		3Rat:rat)
		4Knight:npc(1)
		5PlayerMale:npc(0)
		6Ball:ball)
		7Skull:skull)
		*/
		Sprite[] s;
		switch (x) {
		case 1:
			return Resources.Load<Sprite>("Textures/kobold");
			break;
		case 2:
			s = Resources.LoadAll<Sprite>("Textures/npc");
			return s[2];
			break;
		case 3:
			return Resources.Load<Sprite> ("Textures/rat");
			break;
		case 4:
			s = Resources.LoadAll<Sprite>("Textures/npc");
			return s[1];
			break;
		case 5:
			return Resources.Load<Sprite>("Textures/Player");
		case 6:
			return Resources.Load<Sprite> ("Textures/ball");
			break;
		case 7:
			return Resources.Load <Sprite>("Textures/skull");
			break;
		}
		return null;
	}
	public void UpdateAllNPCVisibility(){
		for(int x = 0; x < npcCount; x++) {
			Vector3 pos = npcs[x].transform.position;
			if (!levelgen.dtilemap.map[(int)pos.x, (int)pos.y].isVisible)
				npcs[x].GetComponent<SpriteRenderer>().enabled = false;
			else if (levelgen.dtilemap.map[(int)pos.x, (int)pos.y].isVisible)
				npcs[x].GetComponent<SpriteRenderer>().enabled = true;
		}
	}
	//public void KillNPC(int index){
		//npcs [index] = 0;
	//}
}
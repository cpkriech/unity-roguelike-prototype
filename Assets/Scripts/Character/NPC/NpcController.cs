﻿using UnityEngine;
using SpriteTile;
using System.Collections.Generic;

public class NpcController : MonoBehaviour {
	//change the creating of the nodes to something else to get the function to work correctly
	public PlayerController playercon;
	public LevelGenerator levelgen;
	public NPCFighter npcfighter;
	public Vector2 currentTarget;
	public Vector2 retreatTarget = new Vector2 (-1, -1);
	protected AI ai;
	public bool isAlive = true;

	public void AssignScripts(LevelGenerator levelgen, PlayerController playercon){
		this.playercon = playercon;
		this.levelgen = levelgen;
		npcfighter = gameObject.GetComponent<NPCFighter> ();
	}

	public void Awake(){
		ai = new AI (this);
	}
	public void MoveNPCTowards(Vector2 target){
		Int2 step = FindPath (target);
		MoveNPCTo (step.x, step.y);
	}
	public void MoveNPCTo(int x, int y) {
		MoveNPCTo(new Vector3 (x,y, transform.position.z));
	}
	public void MoveNPCTo(Vector3 target){
		transform.position = target;
	}
	public int TakeTurn() {
		int energyCost = 0;
		if (isAlive){
			currentTarget = new Vector2 (playercon.transform.position.x, playercon.transform.position.y);
			string action = ai.AiAction (weaponRange:npcfighter.charstats.attackRange);
			string text = "";
			switch (action)
			{
			case "AttackTarget":
				text = gameObject.name + " attacks " +playercon.gameObject.name+".";
				if (npcfighter.charstats.MagicAttack)
					text = gameObject.name + " blasts " +playercon.gameObject.name+" with magic!";
				npcfighter.Attack (playercon.gameObject);
				energyCost = 1000;
				break;
			case "MoveAwayFromTarget":
				MoveNPCTowards(retreatTarget);
				text = gameObject.name + "backs away from "+playercon.gameObject.name+" cautiously.";
				energyCost = 1000;
				break;
			case "MoveTowardsTarget":
				MoveNPCTowards(currentTarget);
				energyCost = 1000;
				break;
			case "MoveRandom":
				int dx = Random.Range (-1, 2);
				int dy = Random.Range (-1, 2);
				if (levelgen.isWalkable((int)transform.position.x + dx, (int)transform.position.y +  dy)){
					MoveNPCTo((int)transform.position.x + dx, (int)transform.position.y +  dy);
					energyCost = 1000;
					break;
				}
				//else do nothing
				energyCost = 1000;
				break;
			case "StandStill":
				energyCost = 1000;
				break;
			}
			if (text.Length > 0) levelgen.ui.textlist.Add (text);
		}
		return energyCost;
	}
	public bool CanMoveToSurroundingTile () {
		return true;
	}
	public bool CanAttack(Vector2 target, string choice = ""){
		if (choice == "currentTarget")
			target = currentTarget;
		if (Distanceto (target) <= npcfighter.charstats.attackRange)
			return true;
		else return false;
	}
	public bool CanMoveTowards(Vector2 target, string choice = ""){
		if (choice == "currentTarget")
				target = currentTarget;
		if (FindPath(target) != new Int2 (-1, -1)) return true;
		else return false;
	}
	public bool CanMoveAwayFrom(Vector2 target, string choice = ""){
		//right choice doesn't do much, will later.
		if (choice == "currentTarget")
						target = currentTarget;
		else if (choice == "retreatTarget")
						target = retreatTarget;
		//Just grabbing a random tile, needs to be reevaluated later.
		if (retreatTarget == new Vector2 (-1, -1)) {
			//if no retreat target, find new good target
			retreatTarget = levelgen.RandomWalkableTile();
		}
		while (FindPath(retreatTarget) == new Int2(-1, -1)){
			retreatTarget = levelgen.RandomWalkableTile();
			}
		return true;
	}

	public bool IsVisibleByPlayer(){
		return levelgen.isInPlayerFOV((int)transform.position.x, (int)transform.position.y);
	}
	public int Distanceto(Vector2 target, string choice = ""){
		if (choice == "currentTarget")
						target = currentTarget;
		return (int)Vector2.Distance (new Vector2 ((int)transform.position.x, (int)transform.position.y), 
		                              new Vector2 ((int)target.x,(int)target.y));
	}

	public Int2 FindPath (Vector2 target) {
		//temporary for ai
		return FindPath ((int)target.x, (int)target.y);
	}
	public Int2 FindPath (int tarx, int tary) {
		//may throw exception if can't find path, but doesn't crash game right now so whatever.
		Node target = new Node( null, new Int2 (tarx, tary), false, 0 ,0);
		//Node[,] mapdata = new Node[levelgen.size_x, levelgen.size_y];

		/*for (int dy = 0; dy < levelgen.size_y; dy++) {
			for (int dx = 0; dx < levelgen.size_x; dx++) {
				mapdata[dx,dy] = new Node ();
				Debug.Log (dx + " " + dy);
			}
		}*/

		//Node node = new Node ();

		//for(int x=0;x<size_x;x++) {
			//for(int y=0;y<size_y;y++) {
				//mapdata[x,y] = new Node();
			//}
		//}

		List<Node> open = new List<Node>();
		List<Node> closed = new List<Node>();
		List<Node> tobeadded = new List<Node>();
		List<int> removeIndexes = new List<int>();
		//add current tile to open list

		Node origin = new Node (null, new Int2((int)transform.position.x, (int)transform.position.y), true, 0, 0);
		open.Add (origin);
		Node currentMinNode = open[0];
		//while open is not empty
		//while (open.Count != 0 || open[0].tile != target.tile) {
		while(true) {
			if (open.Count == 0) {break;}
			//if stuck in a loop, break
			if (open.Count > 1000) {return new Int2 (-1,-1);}
			currentMinNode = open[0];
			//find lowest F in open
			for (int i = 0; i < open.Count; i++) {
				//if i is less than the current lowest found node in open, then new lowest index is equal to i
				if (open[i].F < open[i].F)
					currentMinNode = open[i];
			}
			closed.Add (currentMinNode);
			open.Remove(currentMinNode);
			if (currentMinNode.tile == target.tile) { //current tile = target
				break;
			}
			else {
				tobeadded.Clear ();
				//add surrounding tiles
				Int2 int2 = new Int2 (currentMinNode.tile.x + 1, currentMinNode.tile.y);
				int h = (int)Vector2.Distance (new Vector2(int2.x, int2.y), new Vector2 (tarx, tary));
				if (levelgen.isWalkable (int2.x, int2.y))
					tobeadded.Add (new Node (null, int2, false, currentMinNode.G+10, h));
			
				int2 = new Int2 ((int)currentMinNode.tile.x - 1,(int)currentMinNode.tile.y);
				h = (int)Vector2.Distance (new Vector2(int2.x, int2.y), new Vector2 (tarx, tary));
				if (levelgen.isWalkable (int2.x, int2.y))
					tobeadded.Add (new Node(null, int2, false, currentMinNode.G+10, h));

				int2 = new Int2 ((int)currentMinNode.tile.x,(int)currentMinNode.tile.y-1);
				h = (int)Vector2.Distance (new Vector2(int2.x, int2.y), new Vector2 (tarx, tary));
				if (levelgen.isWalkable (int2.x, int2.y))
					tobeadded.Add (new Node(null, int2, false, currentMinNode.G+10, h));

				int2 = new Int2 ((int)currentMinNode.tile.x,(int)currentMinNode.tile.y + 1);
				h = (int)Vector2.Distance (new Vector2(int2.x, int2.y), new Vector2 (tarx, tary));
				if (levelgen.isWalkable (int2.x, int2.y))
					tobeadded.Add (new Node(null, int2, false, currentMinNode.G+10, h));

				int2 = new Int2 ((int)currentMinNode.tile.x + 1,(int)currentMinNode.tile.y + 1);
				h = (int)Vector2.Distance (new Vector2(int2.x, int2.y), new Vector2 (tarx, tary));
				if (levelgen.isWalkable (int2.x, int2.y))
					tobeadded.Add (new Node(null, int2, false, currentMinNode.G+10, h));

				int2 = new Int2 ((int)currentMinNode.tile.x + 1,(int)currentMinNode.tile.y - 1);
				h = (int)Vector2.Distance (new Vector2(int2.x, int2.y), new Vector2 (tarx, tary));
				if (levelgen.isWalkable (int2.x, int2.y))
					tobeadded.Add (new Node(null, int2,false, currentMinNode.G+10, h));

				int2 = new Int2 ((int)currentMinNode.tile.x - 1,(int)currentMinNode.tile.y + 1);
				h = (int)Vector2.Distance (new Vector2(int2.x, int2.y), new Vector2 (tarx, tary));
				if (levelgen.isWalkable (int2.x, int2.y))
					tobeadded.Add (new Node(null, int2, false, currentMinNode.G+10, h));

				int2 = new Int2 ((int)currentMinNode.tile.x - 1,(int)currentMinNode.tile.y - 1);
				h = (int)Vector2.Distance (new Vector2(int2.x, int2.y), new Vector2 (tarx, tary));
				if (levelgen.isWalkable (int2.x, int2.y))
					tobeadded.Add (new Node(null, int2, false, currentMinNode.G+10, h));
				
				foreach (Node successor in tobeadded) {
					//set parent of node
					successor.parent = currentMinNode;

					//if on closed list, ignore
					bool onClosedList = false;
					for (int i = 0; i < closed.Count; i++) {
						if (closed[i].tile == successor.tile){
							onClosedList = true;
						}
					}

					if (!onClosedList) {
						if(open.Count == 0)
							open.Add (successor);
						else {
							bool onOpenList = false;
							for (int i = 0; i < open.Count; i++) {
								if (successor.tile == open[i].tile) {
									if (successor.G < open[i].G){
										open[i] = successor;
										onOpenList = true;
									}
								}
							}
							if (!onOpenList)
								open.Add(successor);
						}
					}
				}

			}
		}
		if (open.Count == 0) {
			//return target.tile;
			return new Int2 (-1,-1);
		}
		if (currentMinNode.tile == target.tile) {
			Node step = currentMinNode;

			while(step.parent != origin){
				step = step.parent;
			}
			return step.tile;
		}
		else 
			//return target.tile;
			return new Int2 (-1, -1);
	}
	
	public class Node {
		public int G;
		public int H;
		public Node parent;
		public bool added;
		public Int2 tile;

		public Node(Node parent, Int2 tile, bool added, int G, int H){
			this.tile = tile;
			this.parent = parent;
			this.added = added;
			this.G = G;
			this.H = H;
		}

		public int F{
			get{return G + (H * 10);}
		}
	}
}
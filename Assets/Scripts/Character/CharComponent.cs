﻿using System.Collections;
using System;

[Serializable]
public class CharComponent {
	public string name;

	//modifiers for component, can be positive or negative
	public int BaseSpeed;	

	public int BaseAttack;
	public int BaseHealth;
	public int BaseMagic;

	public int ModAttack;
	public int ModHealth;
	public int ModMagic;

	public int ModSpeed;

	//changes ai, higher int = smarter reactions
	public int ModAiIntelligence;

	//Base is for races, is the base that everything else modifies.
	public int BaseLegs;
	public int ModLegs;

	public int BaseArms;
	public int ModArms;

	public int BaseHeads;
	public int ModHeads;

	public int AttackRange;
	//yes or no, claws or not. Multiple bools don't yeild anything extra, besides protective redundancy. 
	public bool Claws;
	public bool DarkVision;
	public bool MagicAttack;

	public CharComponent(string name, 
	                     int BaseSpeed = 0, 
	                     int BaseAttack = 0, int BaseHealth = 0, int BaseMagic = 0,
	                     int ModAttack = 0, int ModSpeed = 0, int ModHealth = 0, int ModMagic = 0,
	                 	 int ModAiIntelligence = 0, 
	                     int BaseLegs = 0, int ModLegs = 0, 
	                     int BaseArms = 0, int ModArms = 0,
	                 	 int BaseHeads = 0, int ModHeads = 0, 
	                     int AttackRange = 0,
	                     bool Claws = false, bool DarkVision = false, bool MagicAttack = false){

		this.name = name;

		this.BaseSpeed = BaseSpeed;

		this.BaseAttack = BaseAttack;
		this.BaseHealth = BaseHealth;
		this.BaseMagic  = BaseMagic;

		this.ModAttack = ModAttack;
		this.ModSpeed  = ModSpeed;
		this.ModHealth = ModHealth;
		this.ModMagic  = ModMagic;

		this.ModAiIntelligence = ModAiIntelligence;

		this.BaseLegs  = BaseLegs;
		this.ModLegs   = ModLegs;
		this.BaseArms  = BaseArms;
		this.ModArms   = ModArms;
		this.BaseHeads = BaseHeads;
		this.ModHeads  = ModHeads;

		this.AttackRange = AttackRange;

		this.Claws = Claws;
		this.DarkVision = DarkVision;
		this.MagicAttack = MagicAttack;
	}
}

﻿using UnityEngine;
using System.Collections;

public class PlayerFighter : CharacterFighter {

	public override void Death ()
	{
		SpriteRenderer spriteren = gameObject.GetComponent<SpriteRenderer> ();
		spriteren.material.SetColor ("_Color", Color.red);
		gameObject.GetComponent<PlayerController>().isAlive = false;
	}
}

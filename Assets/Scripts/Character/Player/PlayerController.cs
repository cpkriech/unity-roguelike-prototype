﻿using UnityEngine;
using System.Collections;
using SpriteTile;
public class PlayerController : MonoBehaviour {
	int dx = 0;
	int dy = 0;
	GameObject currentTarget;
	string nextAction = "Null";
	public LevelGenerator levelgen;

	public bool isAlive = true;

	public bool PlayerInput() {
		//returns true if player took turn;


			/*for(int y = 0; y < levelgen.npcgen.npcCount;y++) {
				GameObject tnpc = levelgen.npcgen.npcs[y];
				if (tnpc.transform.position == rycst2D.transform.position){
					Debug.Log (tnpc.name);
				}
			}*/
		if (isAlive){
			if (Input.GetKeyUp (KeyCode.UpArrow)) 
				dy++;
			else if (Input.GetKeyUp (KeyCode.DownArrow))
				dy--;
			else if (Input.GetKeyUp (KeyCode.LeftArrow))
				dx--;
			else if (Input.GetKeyUp (KeyCode.RightArrow))
				dx++;
			else if (Input.GetKeyUp (KeyCode.Escape)) {
				levelgen.Save();
				//Tile.SetCamera (Camera.main);
				//Tile.NewLevel (new Int2 (size_x, size_y), 0,1f, 0.0f, LayerLock.None);
				//Tile.AddLayer(new Int2 (size_x, size_y), 0,1f, 0.0f, LayerLock.None);
				Tile.EraseLevel();
				Application.LoadLevel (0);
			}
			else if (Input.GetKeyUp (KeyCode.S)) {
				levelgen.Save();
			}

			if (dx != 0 || dy != 0) {
				Vector3 target = new Vector3 (transform.position.x + dx, transform.position.y + dy, transform.position.z);
				if (levelgen.isWalkable((int)target.x, (int)target.y)){
					nextAction = "Move";
					return true;
				}
				else {
					for(int i = 0; i < levelgen.npcgen.npcCount;i++) {
						GameObject npc = levelgen.npcgen.npcs[i];
						if (npc.transform.position == target){
							currentTarget = npc;
							nextAction =  "Attack";
							return true;
						}
					}
					//"You bump into the wall", etc.
					nextAction =  "Null";
					return false;
				}
			}
			else{nextAction =  "Null"; return false;}
		}
		else {nextAction =  "Null"; return false;}
	}
	public int PlayerAction() {
		switch (nextAction) {
		case "Attack":
			PlayerAttack();
			return 1000;
			break;
		case "Move":
			MovePlayerTo();
			return 1000;
			break;
		case "Null":
			return 0;
			break;
		}
		return 0;
	}
	public void MovePlayerTo(int x, int y) {
		MovePlayerTo(new Vector3 (x,y, transform.position.z));
		levelgen.MapUpdate ();
	}
	public void MovePlayerTo(Vector3 target){
		transform.position = target;
	}
	public void MovePlayerTo() {
		//Use only if dx and dy have been set through player input
		MovePlayerTo ((int)gameObject.transform.position.x + dx, (int)gameObject.transform.position.y + dy);
		dx = 0;
		dy = 0;
		nextAction = "Null";
	}
	public void PlayerAttack(){
		string text = "";
		GameObject npc = currentTarget;
		int ran = Random.Range (1, 4);
		switch (ran) {
		case 1:
			text = gameObject.name +" swings her mighty blade into the chest of " + npc.name+".";
			break;
		case 2:
			text = gameObject.name +" cracks " + npc.name + " over the head with the handle of her weapon!";
			break;
		case 3:
			text = gameObject.name +" ducks under a blow from " + npc.name + " and responds with a punishing kick!";
			break;
		}
		levelgen.ui.textlist.Add (text);
		gameObject.GetComponent<PlayerFighter>().Attack(npc);
		dx = 0;
		dy = 0;
		nextAction = "Null";
		currentTarget = null;
	}
	public void AssignScripts(LevelGenerator levelgen){
		this.levelgen = levelgen;
	
	}
}

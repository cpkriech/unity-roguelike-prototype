﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerManager : MonoBehaviour {
	public LevelGenerator levelgen;
	public ComponentManager commgr;
	public MasterControl mc;
	public GameObject player;
	CharacterData playerdata;

	public PlayerManager(){
	}

	public void Start(){
	}

	public void CreateNewPlayer(){
		//Player location needs to be set by creator
		playerdata = CreateNewCharacterData ();
		player = CreateNewNPCObjectFromCharData (playerdata);
		mc.RegisterEntityPlayer ();
	}

	public void LoadPlayer(CharacterData playerdata) {
		this.playerdata = playerdata;
		player = CreateNewNPCObjectFromCharData (playerdata);
		CharacterData.SerVector3 sv = new CharacterData.SerVector3 (0, 0, 0);
		player.transform.position = sv.SerVec3toVec3(playerdata.location);
		mc.RegisterEntityPlayer ();
	}
	
	private GameObject CreateNewNPCObjectFromCharData(CharacterData playerdata){
		player = GameObject.Find("Player");
		SpriteRenderer spriteren = player.GetComponent<SpriteRenderer> ();
		spriteren.sortingLayerName = "Characters";
		spriteren.sprite = AssignSprites (playerdata.spriteIDnum);
		PlayerController playercon = player.GetComponent<PlayerController> ();
		PlayerFighter playerfighter = player.AddComponent<PlayerFighter> ();
		CharacterStats charstats = player.AddComponent<CharacterStats> ();
		playercon.AssignScripts (levelgen);
		playerfighter.AssignScripts ();
		charstats.AssignComponents (playerdata.components);
		if (playerdata.currentHealth > 0)
			charstats.currenthealth = playerdata.currentHealth;
		charstats.energy = playerdata.energy;
		return player;
	}
	private CharacterData CreateNewCharacterData() {
		List<CharComponent> components = new List<CharComponent>();
		components.Add (commgr.GetRandomComp ());
		components.Add (commgr.GetClass (commgr.CLASS_SOLDIER));
		components.Add (commgr.GetSpecies(commgr.SPECIES_ELF));
		CharacterData newplayerdata = new CharacterData();
		string name = "Helen Strong";
		Vector3 location = Vector3.zero;
		int spriteIDnum = 5;
		int energy = -1;
		int chealth = -1;
		newplayerdata = new CharacterData(name, location, 5, -1, chealth, energy, components);
		return newplayerdata;
	}
	public CharacterData GetCharacterData(){
		return new CharacterData (playerdata.name, player.transform.position, playerdata.spriteIDnum, -1,
		                          player.GetComponent<CharacterStats> ().currenthealth, player.GetComponent<CharacterStats> ().energy,
		                          player.GetComponent<CharacterStats> ().components);
	}
	public Sprite AssignSprites(int x){
		/*FileReader.SpriteID sID = fr.getSpriteID(x);
		if (sID.index != -1) {
			Sprite[] s = Resources.LoadAll<Sprite>(sID.path);
			return s[sID.index];
		}
		else {
			return Resources.Load<Sprite>(sID.path);
		}
		*/
		Sprite[] s;
		switch (x) {
		case 1:
			return Resources.Load<Sprite>("Textures/kobold");
			break;
		case 2:
			s = Resources.LoadAll<Sprite>("Textures/npc");
			return s[2];
			break;
		case 3:
			return Resources.Load<Sprite> ("Textures/rat");
			break;
		case 4:
			s = Resources.LoadAll<Sprite>("Textures/npc");
			return s[1];
			break;
		case 5:
			return Resources.Load<Sprite>("Textures/Player");
		case 6:
			return Resources.Load<Sprite> ("Textures/ball");
			break;
		case 7:
			return Resources.Load <Sprite>("Textures/skull");
			break;
		}
		return null;
	}
}

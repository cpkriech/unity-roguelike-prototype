﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public abstract class CharacterFighter : MonoBehaviour {
	
	public CharacterStats charstats;

	public void Attack(GameObject target){
		int dam = 0;
		int attack = charstats.attack;
		if (charstats.MagicAttack)
			attack = charstats.magic;
		for (int x = 0; x < (attack) ; x++){
			//dam = attack + 4 d 6
			dam = dam + UnityEngine.Random.Range (1,5);
		}
		CharacterFighter targetfighter = target.GetComponent<CharacterFighter> ();
		targetfighter.TakeDamage (dam);
	}
	public abstract void Death ();

	public void AssignScripts() {
		charstats = gameObject.GetComponent<CharacterStats> ();
	}

	public void TakeDamage(int dam){
		charstats.currenthealth = charstats.currenthealth - dam;
		if (charstats.currenthealth <= 0)
			Death ();
	}
}

﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class CharacterStats : MonoBehaviour {
	//Base is calculated by components, these stats are current values (base +- modifiers)
	//temporary (will be replaced by different system)

	//All stats are calculated (for both player and npc). Each class can ignore what it wants.
	public List<CharComponent> components;

	public int maxhealth;
	public int currenthealth;
	
	public int health;
	public int attack;
	public int speed;
	public int magic;

	public int aiIntelligence;
	
	public int legs;
	public int arms;
	public int heads;
	
	public bool Claws;
	public bool DarkVision;
	public bool MagicAttack;

	public int attackRange;

	public int spriteIDnum;
	public int index;

	//used for actions
	public int energy;

	public void InitilizeStats(){
		RefreshStats ();
		energy = 0;
	}

	protected void RefreshStats () {
		foreach (CharComponent comp in components) {
			if (comp.BaseArms != 0) arms = comp.BaseArms;
			if (comp.BaseLegs != 0) legs = comp.BaseLegs;
			if (comp.BaseHeads != 0) heads = comp.BaseHeads;


			if(comp.BaseSpeed !=  0)speed = comp.BaseSpeed;
			if(comp.BaseAttack != 0)attack = comp.BaseAttack;
			if(comp.BaseMagic !=  0)magic = comp.BaseMagic;
			if(comp.BaseHealth != 0)health = comp.BaseHealth;
			
			if (comp.Claws) Claws = true;
			if (comp.DarkVision) DarkVision = true;
			if (comp.MagicAttack) MagicAttack = true;
			maxhealth = 50 + 10 * health;
		}
		foreach (CharComponent comp in components) {
			arms  = arms  + comp.ModArms;
			legs  = legs  + comp.ModLegs;
			heads = heads + comp.ModHeads;
			
			health = health + comp.ModHealth;
			attack = attack + comp.ModAttack;
			speed  = speed  + comp.ModSpeed;
			magic  = magic  + comp.ModMagic;

			if (comp.AttackRange > attackRange) attackRange = comp.AttackRange;
			if (attackRange < 1) attackRange = 1;
			aiIntelligence = aiIntelligence + comp.ModAiIntelligence;
		}

		currenthealth = maxhealth;
	}
	public void AssignComponents(List<CharComponent> components) {
		this.components = components;
		InitilizeStats ();
	}
}

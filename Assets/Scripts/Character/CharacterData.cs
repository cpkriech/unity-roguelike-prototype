﻿using UnityEngine;
using System.Collections.Generic;
using System;

[System.Serializable]
 public class CharacterData {
	
	public string name;
	public SerVector3 location;
	public int spriteIDnum;
	public int index;
	public int currentHealth;
	public int energy;
	public List<CharComponent> components;
		
	public CharacterData (string name, Vector3 location, int spriteIDnum, int index, int currentHealth, 
	                     int energy, List<CharComponent> components){
		SerVector3 sv = new SerVector3 (0, 0, 0);
		this.name = name;
		this.location = sv.Vec3toSerVec3 (location);
		this.spriteIDnum = spriteIDnum;
		this.index = index;
		this.currentHealth = currentHealth;
		this.energy = energy;
		this.components = components;
	}
	public CharacterData(){
	}
	[System.Serializable]
	public class SerVector3 {
		public float x;
		public float y;
		public float z;

		public SerVector3(float x, float y, float z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public SerVector3 Vec3toSerVec3(Vector3 vector) {
			SerVector3 svector = new SerVector3 (vector.x, vector.y, vector.z);
			return svector;
		}

		public Vector3 SerVec3toVec3(SerVector3 servector) {
			Vector3 vector = new Vector3 (servector.x, servector.y, servector.z);
			return vector;
		}
	}
}

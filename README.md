# README #

This is a very small unity roguelike project that I moved on from. It was initially a way for me to learn 2D unity. The current version is not very stable (it can't be compiled from this project, as I used some asset store libraries, and I couldn't add those to the source code.)

The game can be played from the zip file in the main folder in the source code.

# CONTROLS #

Arrow keys for movement and attacking. Clicking on an npc will bring up a short description.

There is only one level, and the enemies are not balanced at all. The trait system used to create monsters and the players is being used in my next project, BlobRL.